const {Schema, model} = require('mongoose');

const Note = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },

  completed: {
    type: Boolean,
    default: false,
  },

  text: {
    type: String,
    required: true,
  },

  createdDate: {
    type: Date,
    default: Date.now,
  },
});

module.exports = model('Note', Note);
