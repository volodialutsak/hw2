const {Schema, model} = require('mongoose');
const passwordHasher = require('../helpers/passwordHasher');

const User = new Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },

  password: {
    type: String,
    required: true,
  },

  createdDate: {
    type: Date,
    default: Date.now,
  },
});

// Hash password when register new user
User.post('validate', async function(user, next) {
  user.password = await passwordHasher(user.password);
  next();
});


module.exports = model('User', User);
