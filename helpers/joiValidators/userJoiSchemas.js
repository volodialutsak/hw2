const Joi = require('joi');

const joiPassword = Joi.string().required();

// This validator is more suitable for a password, but we can't use it because
// we don't know which password format the bot uses.
//
// const joiPassword = Joi.string()
//     .pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)
//     .required()
//     .messages({
//       'string.pattern.base': 'Passwords must contain: ' +
//       'a minimum of 1 lower case letter \[a-z\] and ' +
//       'a minimum of 1 upper case letter \[A-Z\] and ' +
//       'a minimum of 1 numeric character \[0-9\] and ' +
//       'must be at least 8 characters in length, but can be much longer',
//     });

const credentialsSchema = Joi.object({
  username: Joi.string()
      .trim()
      .required(),

  password: joiPassword,
});

const passwordsSchema = Joi.object({
  oldPassword: joiPassword,
  newPassword: joiPassword,
});

module.exports = {credentialsSchema, passwordsSchema};
