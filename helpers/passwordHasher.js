const bcrypt = require('bcrypt');
const asyncHandler = require('express-async-handler');

const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);

module.exports = asyncHandler( async (password) => {
  return bcrypt.hash(password, salt);
});
