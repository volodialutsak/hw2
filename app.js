require('dotenv').config();
const logger = require('./middleware/utils/logger');
const express = require('express');
const morgan = require('morgan');
const apiErrorHandler = require('./middleware/errorHandler');
const connectDB = require('./config/connectDB');
const tokenParser = require('./middleware/tokenParser');
const cors = require('./middleware/cors');
const config = require('./config/config');

connectDB();

const app = express();

// CORS
app.use(cors);

// Parsers
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(tokenParser);

// Logging
app.use(morgan('dev'));

// Routes
app.use('/api/auth', require('./routes/authRouter'));
app.use('/api/users', require('./routes/usersRouter'));
app.use('/api/notes', require('./routes/notesRouter'));

// Error handler
app.use(apiErrorHandler);

app.listen(config.PORT, (req, res) => {
  logger.info(`
    ################################################
          Server is listening on port: ${config.PORT}
    ################################################
  `);
}).on('error', (err) => {
  logger.error(err);  
  process.exit(1);
});
