import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {AddNoteComponent} from './components/add-note/add-note.component'
import { LoginComponent } from './components/login/login.component';
import { NotesComponent } from './components/notes/notes.component';
//import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},
  { path: 'notes', component: NotesComponent, canActivate: [AuthGuard]},
  { path: 'addNote', component: AddNoteComponent, canActivate: [AuthGuard]},
  //{ path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},  
  { path: '**', redirectTo: 'login'}
]

@NgModule({  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }