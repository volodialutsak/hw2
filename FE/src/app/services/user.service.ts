import { Injectable } from '@angular/core';
import { Observable, of, Subject, firstValueFrom } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { User } from '../user';
import { Credentials } from 'src/app/credentials';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _currentUser: User | undefined;  
  userIsLoggedIn = new Subject();
  
  private serverURL: string = 'http://localhost:8080';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }
  
  set currentUser(user: User | undefined) {
    this._currentUser = user;
    if (user) {
      localStorage.setItem('currentUser', JSON.stringify(user));
    } else {
      localStorage.removeItem('currentUser');
    }    
  }

  get currentUser(): User | undefined {
    if (this._currentUser === undefined) {      
      let jsonUserData = localStorage.getItem('currentUser');
      if (jsonUserData) {
        this.currentUser = JSON.parse(jsonUserData);        
      }      
    }
    return this._currentUser;
  }

  public login(credentials: Credentials): Observable<any> {
    return this.http.post<any>(this.serverURL + '/api/auth/login', credentials).pipe(
          tap(_ => this.log('fetched user')),
          catchError(this.handleError<any>('login'))
    );
  }

  public register(credentials: Credentials): Observable<any> {
    return this.http.post<any>(this.serverURL + '/api/auth/register', credentials).pipe(
          tap(_ => this.log('registered user')),
          catchError(this.handleError<any>('register'))
    );
  }

  public getUser(token: string): Observable<User> {

    return this.http.get<User>(
      this.serverURL + '/api/users/me',
      {
        headers: {
        authorization: 'JWT ' + token,
        }
      },
    ).pipe(      
      tap(_ => this.log('fetched user')),
      catchError(this.handleError<User>('getUser'))
    );
  }

  public updateUser(user: User): Observable<any> {
    const userData = {...user};
    delete userData._id;
    return this.http.put(this.serverURL + '/users/' + user._id, userData, this.httpOptions).pipe(
      tap(_ => this.log(`updated user id=${user._id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  // private async asyncGetUsers() {
  //   return await firstValueFrom(this.getUsers());
  // } 

  // public addUser(email: string, password: string): Observable<User> {
  //   let users: User[] = [];
  //   this.asyncGetUsers().then(res => users = res);
  //   let id: number = 1;
  //   if (users) {
  //     id = Math.max(...users.map<number>(user => user.id!)) + 1
  //   }
    
    // const newUser: User = {
    //   _id,
    //   username: '',
     
      
    // }
    
  //   return this.http.post<User>(this.serverURL + '/users', newUser, this.httpOptions).pipe(
  //     tap(_ => this.log(`added new user id=${newUser.id}`)),
  //     catchError(this.handleError<any>('addUser'))
  //   );
  // }

   /**
 * Handle Http operation that failed.
 * Let the app continue.
 *
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
        console.error(error);  
        this.log(`${operation} failed: ${error.error}`);

        // Let the app keep running by returning an empty result.
        return of(error.error as T);
      };
    }
  
    private log(message: string) {
      console.log(`UserService: ${message}`);
    }
}