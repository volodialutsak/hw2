import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Note } from '../note';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class NoteService {  
  
  private serverURL: string = 'http://localhost:8080';
  
  constructor(private http: HttpClient,  private userService: UserService) { }

  public getNotes(): Observable<any> {
    return this.http.get<any>(
      this.serverURL + '/api/notes',
      {
        headers: {
        authorization: 'JWT ' + this.userService.currentUser?.token,
        }
      },
    ).pipe(      
      tap(_ => this.log('fetched notes')),
      catchError(this.handleError<any>('getNotes'))
    );    
  }

  public addNote(text: string): Observable<any> {
    return this.http.post<any>(
      this.serverURL + '/api/notes',
      {text},
      {
        headers: {
        authorization: 'JWT ' + this.userService.currentUser?.token,
        }
      },
    ).pipe(      
      tap(_ => this.log('add notes')),
      catchError(this.handleError<any>('addNotes'))
    );
  } 

   /**
 * Handle Http operation that failed.
 * Let the app continue.
 *
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
    private handleError<T>(operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
        console.error(error);  
        this.log(`${operation} failed: ${error.message}`);

        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }
  
    private log(message: string) {
      console.log(`NoteService: ${message}`);
    }
}
