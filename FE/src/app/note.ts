export interface Note {
    _id: string,
    userId: string,
    completed: boolean,
    text: string,
    createdDate: Date,
}