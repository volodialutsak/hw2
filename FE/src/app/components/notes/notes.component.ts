import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { UserService } from 'src/app/services/user.service';
import { Note } from 'src/app/note';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {  
  notes!: Note[];  

  constructor(
    private userService: UserService,
    private navService: NavigationService,
    private noteService: NoteService) { }

  ngOnInit(): void {
    this.navService.currentNavigationElement.next('notes');
    this.getNotes();
  }

  getNotes(): void {
    this.noteService.getNotes().subscribe(res => {
      this.notes = res.notes;
      console.log(res.notes);             
    });
  }

  // searchFriends(data: string) {
  //   this.searchTerm = data.toLowerCase();
  //   this.searchedUsers = this.users.filter(user => {
  //     const notCurrentUser = user !== this.userService.currentUser;
  //     const notFriend = !this.friendUsers.includes(user);
  //     const isSearchTermInUserName = user.username.toLowerCase().includes(this.searchTerm);

  //     return notCurrentUser && notFriend && isSearchTermInUserName;
  //   })
  // }

  // addFriend(friend: User) {    
  //   const user = {...this.userService.currentUser} as User;
  //   user.friends.push(friend.id!);

  //   this.userService.updateUser(user).subscribe(user => {
  //     this.userService.currentUser = user;
  //     this.friendUsers.push(friend);
  //     this.searchFriends(this.searchTerm);
  //   })
    
  // }

  removeNote(note: Note) {    
    // const user = {...this.userService.currentUser} as User;
    // user.friends = user.friends?.filter(id => id !== friend.id);

    // this.userService.updateUser(user).subscribe(user => {
    //   this.userService.currentUser = user;
    //   this.friendUsers = this.friendUsers.filter(user => user !== friend);
    //   this.searchFriends(this.searchTerm);
    // })    
  }
}