import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/user';
import { EMAIL_REG_EXP } from 'src/email-validation-regExp';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Credentials } from 'src/app/credentials';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {  
  user!: User;
  confirmValue: string = '';
  emailIsValid: boolean = false;
  isSignIn: boolean = true;
  loginFormIsHidden = false;
  alertIsHidden = true;
  alertMessage: string = '';
  credentials: Credentials = {username: '', password: ''};

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    if (this.userService.currentUser) {
      this.router.navigate(['notes']);
    }    
  }  

  onSubmit() {
    if(this.isSignIn) {
      this.signIn(this.credentials);
    } else {
      this.signUp(this.credentials);
    }    
  }

  signIn(credentials: Credentials) {    
    this.userService.login(credentials).subscribe(obj => {
      console.log(obj);
      if (obj?.jwt_token) {
        this.userService.getUser(obj.jwt_token).subscribe(user => {
          if (user) {
            user.token = obj.jwt_token;
            this.userService.currentUser = user;
            this.userService.userIsLoggedIn.next(true);
            this.router.navigate(['notes']);
          } else {
              this.showAlertMessage(`${obj.message}`);
          }
        })
      } else {
        this.showAlertMessage(`${obj.message}`);
      }
    });
  }

  signUp(credentials: Credentials) {
    if (this.credentials.password.length < 8) {
      return this.showAlertMessage('Password has to be at least 8 characters long!');
    }
    if (this.credentials.password !== this.confirmValue) {
      return this.showAlertMessage('Passwords are not equal! Please try again.');      
    }

    this.userService.register(credentials).subscribe(obj => {
      console.log(obj);
      if (obj?.message === 'Success') {        
        this.showAlertMessage('NEW USER REGISTERED!');
        this.credentials.password = '';
        this.confirmValue = '';
      } else {
        this.showAlertMessage(`${obj.message}`);
      }
    });     
  }

  showAlertMessage(message: string) {
    this.alertMessage = message;
    this.alertIsHidden = false;
  }

  // onEmailChange() {    
  //   const validEmail: boolean = EMAIL_REG_EXP.test(this.usernameValue);    

  //   if (validEmail) {
  //     this.emailIsValid = true;      
  //   } else {
  //     this.emailIsValid = false;      
  //   }
  // }

  switchLogin() {
    this.isSignIn = !this.isSignIn;
    this.credentials.password = '';
    this.confirmValue = '';
    this.alertIsHidden = true;
  }
}