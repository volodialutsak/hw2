import { Component, OnInit } from '@angular/core';
import {NoteService} from '../../services/note.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {
  text: string = '';

  constructor(private noteService: NoteService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.noteService.addNote(this.text).subscribe(res => {
      this.text = '';
    });
  }

}
