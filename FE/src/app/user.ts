export interface User {
    _id?: string,
    username: string,   
    createdDate: Date,  
    token?:string,
}