const config = {
  PORT: process.env.PORT || 8080,
  NODE_ENV: process.env.ENV || 'dev',
  JWT_SECRET: 'topsecret',
  MONGO_URI: 'mongodb+srv://Volodymyr:Cloudmongodb00@cluster0.wbesl.mongodb.net/storybooks?retryWrites=true&w=majority',
};

module.exports = config;
