const ApiError = require(`./utils/ApiError`);

const tokenParser = (req, res, next) => {
  const authorization = req.headers.authorization;
  if (!authorization) {
    req.token = null;
    return next();
  }

  if (!authorization.startsWith('JWT')) {
    throw ApiError.badRequest(`JWT token is allowed only`);
  };

  const bearerToken = authorization.split(' ')
      .filter((item) => item !== '');

  req.token = bearerToken[1];
  next();
};

module.exports = tokenParser;
