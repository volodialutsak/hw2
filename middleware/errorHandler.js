const ApiError = require('./utils/ApiError');
const logger = require('./utils/logger');
const Joi = require('joi');

/**
 * Error handler
 * @param {error} err
 * @param {request} req
 * @param {response} res
 * @param {cb} next
 */
function apiErrorHandler(err, req, res, next) {
  if (err instanceof ApiError) {
    logger.info(err.message);
    res.status(err.code).json({message: err.message});
    return;
  }

  if (Joi.isError(err)) {
    logger.info(err.details);
    res.status(400).json(
        {message: err.details[0].message},
    );
    return;
  }

  logger.error(err);
  res.status(500).json({message: `Server error: ${err}`});
}

module.exports = apiErrorHandler;
