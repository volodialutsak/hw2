const jwt = require(`jsonwebtoken`);
const bcrypt = require('bcrypt');
const User = require(`../models/User`);
const asyncHandler = require('express-async-handler');
const ApiError = require(`../middleware/utils/ApiError`);
const config = require('../config/config');

const authHandler = asyncHandler( async (req, res, next) => {
  const token = req.token;

  if (!token) {
    throw ApiError.badRequest(`Unauthorized request. Missing token`);
  }

  const decoded = jwt.verify(token, config.JWT_SECRET);

  const user = await User.findById(decoded._id);
  if (!user) {
    throw ApiError.badRequest(`Unauthorized request. Bad token`);
  }

  const isValidPassword = await bcrypt.compare(decoded.password, user.password);
  if (!isValidPassword) {
    throw ApiError.badRequest(`Unauthorized request. Bad token`);
  }

  req.user = user;
  next();
});

module.exports = authHandler;
