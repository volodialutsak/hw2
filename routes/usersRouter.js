const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const authHandler = require('../middleware/authHandler');
const {
  // getUser,
  deleteUser,
  checkPasswords,
  changePassword,
} = require('../services/userService');

// eslint-disable-next-line new-cap
const router = Router();

// @desc   User can request only his own profile info
// @route  GET /api/users/me
// @access Protected
router.get('/me', authHandler, asyncHandler( async (req, res) => {
  const user = req.user;

  const {_id, username, createdDate} = user;

  res.status(200).json({user: {_id, username, createdDate}});
}));

// @desc   User can delete only his own profile info
// @route  DELETE /api/users/me
// @access Protected
router.delete('/me', authHandler, asyncHandler( async (req, res) => {
  const user = req.user;
  await deleteUser(user._id);

  res.status(200).json({message: 'Success'});
}));

// @desc   Change user's password
// @route  PATCH /api/users/me
// @access Protected
router.patch('/me', authHandler, asyncHandler( async (req, res) => {
  const user = req.user;
  const {newPassword} = checkPasswords(req.body);
  await changePassword(user, newPassword);

  res.status(200).json({message: 'Success'});
}));

module.exports = router;
