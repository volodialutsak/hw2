const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const {
  checkAndFormatUserData,
  createUser,
  checkCredentials,
  generateToken,
  getUser,
} = require('../services/authService');

// eslint-disable-next-line new-cap
const router = Router();

// @desc   Create new profile
// @route  POST /api/auth/register
// @access Unprotected
router.post('/register',
    asyncHandler(async (req, res) => {
      const userDTO = await checkAndFormatUserData(req.body);
      await createUser(userDTO);

      res.status(200).json({message: 'Success'});
    }),
);

// @desc   Login into the system, generate token
// @route  POST /api/auth/login
// @access Unprotected
router.post('/login',
    asyncHandler(async (req, res) => {
      const userDTO = await checkCredentials(req.body);
      const {username, password} = userDTO;
      const user = await getUser(userDTO);
      const {_id} = user;
      const token = generateToken({_id, username, password});

      res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    }),
);

module.exports = router;
