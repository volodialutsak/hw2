const {Router} = require('express');
const asyncHandler = require('express-async-handler');
const authHandler = require('../middleware/authHandler');

const {
  getNotes,
  checkQueryParams,
  checkNote,
  checkAndFormatNote,
  addNote,
  checkNoteId,
  getNote,
  updateNote,
  patchNote,
  deleteNote,
} = require('../services/noteService');

// eslint-disable-next-line new-cap
const router = Router();

// @desc   Retrieve the list of notes for authorized user
// @route  GET /api/notes
// @access Protected
router.get('/', authHandler, asyncHandler( async (req, res) => {
  const {offset, limit} = checkQueryParams(req.query);
  const {_id: userId} = req.user;
  const notes = await getNotes(userId, offset, limit);

  res.status(200).json({
    offset,
    limit,
    count: notes.length,
    notes,
  });
}));

// @desc   Add Note for User
// @route  POST /api/notes
// @access Protected
router.post('/', authHandler, asyncHandler( async (req, res) => {
  const {_id: userId} = req.user;
  const note = checkAndFormatNote(req.body, userId);

  await addNote(note);

  res.status(200).json({message: 'Success'});
}));

// @desc   Get user's note by id
// @route  GET /api/notes/:id
// @access Protected
router.get('/:id', authHandler, asyncHandler( async (req, res) => {
  const {id: noteId} = checkNoteId(req.params.id);
  const {_id: userId} = req.user;
  const note = await getNote(userId, noteId);

  res.status(200).json({note});
}));

// @desc   Update user's note by id
// @route  PUT /api/notes/:id
// @access Protected
router.put('/:id', authHandler, asyncHandler( async (req, res) => {
  const {id: noteId} = checkNoteId(req.params.id);
  const {text} = checkNote(req.body);
  const {_id: userId} = req.user;
  await getNote(userId, noteId);
  await updateNote(noteId, text);

  res.status(200).json({message: 'Success'});
}));

// @desc   Check/uncheck user's note by id,
// value for completed field should be changed to opposite
// @route  PATCH /api/notes/:id
// @access Protected
router.patch('/:id', authHandler, asyncHandler( async (req, res) => {
  const {id: noteId} = checkNoteId(req.params.id);
  const {_id: userId} = req.user;
  const {completed} = await getNote(userId, noteId);
  await patchNote(noteId, completed);

  res.status(200).json({message: 'Success'});
}));

// @desc   Delete user's note by id
// @route  DELETE /api/notes/:id
// @access Protected
router.delete('/:id', authHandler, asyncHandler( async (req, res) => {
  const {id: noteId} = checkNoteId(req.params.id);
  const {_id: userId} = req.user;
  await getNote(userId, noteId);
  await deleteNote(noteId);

  res.status(200).json({message: 'Success'});
}));

module.exports = router;
