const jwt = require(`jsonwebtoken`);
const bcrypt = require('bcrypt');
const User = require(`../models/User`);
const ApiError = require(`../middleware/utils/ApiError`);
const Joi = require('joi');
const {credentialsSchema} = require('../helpers/joiValidators/userJoiSchemas');
const config = require('../config/config');

const checkAndFormatUserData = async (userData) => {
  // Credentials validation
  userData = Joi.attempt(userData, credentialsSchema);

  const {username} = userData;

  // Check that username is unique
  const user = await User.findOne({username});
  if (user) {
    throw ApiError.badRequest(
        `User with username '${username}' already exists`,
    );
  }

  return new User(userData);
};

const checkCredentials = (credentials) => {
  return Joi.attempt(credentials, credentialsSchema);
};

const createUser = async (user) => {
  return await User.create(user);
};

const getUser = async (credentials) => {
  const {username, password} = credentials;

  const user = await User.findOne({username});
  if (!user) {
    throw ApiError.badRequest(`User with username '${username}' is not found`);
  }

  const result = await bcrypt.compare(password, user.password);
  if (!result) {
    throw ApiError.badRequest(`Wrong password`);
  }

  return user;
};

const generateToken = ({_id, username, password}) => {
  return jwt.sign({_id, username, password}, config.JWT_SECRET, {
    expiresIn: `30d`,
  });
};

module.exports = {
  checkAndFormatUserData,
  checkCredentials,
  createUser,
  getUser,
  generateToken,
};
