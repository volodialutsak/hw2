const Joi = require('joi');
const Note = require('../Models/Note');
const ApiError = require('../middleware/utils/ApiError');

const {
  queryParamsSchema,
  textSchema,
  noteIdSchema,
} = require('../helpers/joiValidators/noteJoiSchemas');


const checkQueryParams = ({offset = 0, limit = 0}) => {
  return Joi.attempt({offset, limit}, queryParamsSchema);
};

const getNotes = async (userId, offset, limit) => {
  return await Note.find({userId})
      .skip(offset)
      .limit(limit)
      .select('-__v');
};

const checkAndFormatNote = (bodyData, userId) => {
  const {text} = checkNote(bodyData);
  return new Note({userId, text});
};

const checkNote = (bodyData) => {
  return Joi.attempt(bodyData, textSchema);
};

const addNote = async (note) => {
  return await Note.create(note);
};

const checkNoteId = (id) => {
  console.log(id);
  return Joi.attempt({id}, noteIdSchema);
};

const getNote = async (userId, noteId) => {
  const note = await Note.findById(noteId).select('-__v');
  console.log(note.userId.toString());
  console.log(userId);

  if (!note) {
    throw ApiError.badRequest(`The note with id:${noteId} is not found`);
  }

  if ( note.userId.toString() !== userId.toString() ) {
    throw ApiError.badRequest(`Unauthorized request. Bad token`);
  }

  return note;
};

const updateNote = async (noteId, text) => {
  return await Note.findByIdAndUpdate(noteId, {$set: {text}});
};

const patchNote = async (noteId, completed) => {
  return await Note.findByIdAndUpdate(noteId, {$set: {completed: !completed}});
};

const deleteNote = async (noteId) => {
  return await Note.findByIdAndDelete(noteId);
};

module.exports = {
  checkQueryParams,
  getNotes,
  checkNote,
  checkAndFormatNote,
  addNote,
  checkNoteId,
  getNote,
  updateNote,
  patchNote,
  deleteNote,
};
