const Joi = require('joi');
const User = require(`../models/User`);
const {passwordsSchema} = require('../helpers/joiValidators/userJoiSchemas');
const passwordHasher = require('../helpers/passwordHasher');

const deleteUser = async (id) => {
  return await User.findByIdAndRemove(id);
};

const checkPasswords = (passwords) => {
  return Joi.attempt(passwords, passwordsSchema);
};

const changePassword = async (user, newPassword) => {
  await User.findByIdAndUpdate(user._id,
      {$set: {password: await passwordHasher(newPassword)}});
};

module.exports = {deleteUser, checkPasswords, changePassword};
